$machines = get-content "PATH GOES HERE"
$File_Origin = 'PATH GOES HERE'
$File_Dest = 'PATH GOES HERE'

foreach ($machine in $machines) {
$session = New-PSSession -ComputerName $machine
Copy-Item $File_Origin -ToSession $session -Destination $File_Dest -Recurse
Invoke-command -ComputerName $machine -ScriptBlock {
   msiexec /qn /i 'PATH GOES HERE' GENERATE_CONFIG=true LICENSE_KEY='KEY-GOES-HERE'
}

Remove-PSSession $session

}
