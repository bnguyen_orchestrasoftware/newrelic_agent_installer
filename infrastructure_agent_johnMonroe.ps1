function Is-Current-User-Admin() {
return ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
}

function write-event($source, $eventid, $entrytype, $message) {
$SourceExists = [System.Diagnostics.EventLog]::SourceExists("New Relic Agent Install")
if (! $SourceExists) {
New-EventLog -LogName Application -Source "New Relic Agent Install"
}

write-host "source: ", $source, "eventid: ", $eventid, "message: ", $message
Write-EventLog –LogName Application –Source $source –EntryType $entrytype –EventID $eventid –Message $message
}

function check_temp_new_relic_path(){
if (!(test-path -path $pathname)) {
new-item -ItemType directory -Path $pathname -force
}
if (!(test-path $pathname)){
write-message "Could not create c:\temp\new_relic_agent. Please check manually"
}

$message = 'Ran the check_temp_new_relic_path Function'
write-event -source 'New Relic Agent Install' -eventid '7020' -entrytype 'Information' -message $message
return
}

function download_files(){
if (!(test-path -path $pathname)) {
new-item -ItemType directory -Path $path -force
}
cd $pathname
$output = Invoke-WebRequest -uri $msiFullPath -outfile $msiFile
write-host $output
}

function Install-MSIFile {
{Param (
[string]$msiFile,
[string]$pathName,
[string]$newRelicLicensekey
);
}
$fullpath = $pathName+$msiFile

$arguments = @(
"/i"
"$fullPath"
"/qn AcceptEndUserLicenseAgreement=1"
"/norestart"
"/lv c:\temp\new_relic_agent\install.log"
"USE_SETTINGS_FROM_AD=1"
"NR_LICENSE_KEY=" + $newRelicLicenseKey
"NR_PROXY_ADDRESS=ChangeMe"
)

write-host $arguments

if ($targetDir){
if (!(Test-Path $targetDir)){
throw "Path to the Installation Directory $($targetDir) is invalid. Please supply a valid installation directory"
}
$arguments += INSTALLDIR="$targetDir"
}

Write-Verbose "Installing $msiFile....."
$process = Start-Process -FilePath msiexec.exe -ArgumentList $arguments -Wait -PassThru
if ($process.ExitCode -eq 0){
Write-Verbose "$msiFile has been successfully installed"
}
else {
Write-Verbose "installer exit code $($process.ExitCode) for file $($msifile)"
}
}

function clean_temp_files($msiFile){
if (test-path $pathName){
write-host "finding path"
$file = Get-ChildItem -Path $msiFile
remove-item $file
set-location -path c:\temp
get-location
remove-item -path $pathName
}
$message = 'Ran the clean_temp_files Function'
write-event -source 'New Relic Agent Install' -eventid '7029' -entrytype 'Information' -message $message
return
}

function CheckAgentVersion() {
$rtn = get-wmiobject win32_operatingsystem -computer localhost | select-object -expandproperty OSArchitecture
write-host "Returning X64Version" $rtn
if(!($rtn = '64-bit')){
write-host "This is only for 64bit systems"
exit
}
return $rtn
}

function get-nr-license-key {
$newRelicLicenseKey = read-host 'Input your License key'
return $newRelicLicenseKey
}

function edit_yml_file {
    $strings = @()
    $strings += 'license_key: ' + $newRelicLicenseKey
    $strings += 'proxy: CHANGEME'
    $strings |set-content "C:\Program Files\New Relic\newrelic-infra\newrelic-infra.yml"
}

function start_service {
    start-service -name newrelic-infra
}

#Variables
$msiPath = 'https://download.newrelic.com/infrastructure_agent/windows/'
$msiFile = 'newrelic-infra.msi'
$msiFullPath = $msiPath + $msiFile
$pathName = 'c:\temp\new_relic_agent\'

#system Checks
$isAdmin = Is-Current-User-Admin
CheckAgentVersion

#Prompt user for NR License Key
$newRelicLicenseKey = get-nr-license-key

#Call Functions
check_temp_new_relic_path
download_files
Install-MSIFile -msiFile $msiFile -newRelicLicenseKey $newRelicLicenseKey
edit_yml_file
start_service
clean_temp_files
